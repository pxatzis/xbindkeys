# Elementary OS map multitasking view to mouse button

## Install xbindkeys xautomation
```bash
sudo apt-get install xbindkeys xautomation
```

## Find mouse button number

```bash
pxatzis@elite:~$ xev | grep button
    state 0x0, button 1, same_screen YES
    state 0x100, button 1, same_screen YES
    state 0x0, button 9, same_screen YES
```
Button no 9 will be used

### xbindkeys config

Multitasking view keyboard shortcut for Elementary OS is [Win key]+W Win key is also referred to as Super key

```bash
 cat .xbindkeysrc
# Button 9 sends Super + w
"xte 'keydown Super_L' 'key w' 'keyup Super_L'"
b:9
```

Start and test with

```bash
killall xbindkeys
xbindkeys
```

### Auto startup xbindkeys
System settings > Applications > Startup > Add xbindkeys as a startup application.
